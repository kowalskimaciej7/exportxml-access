<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

<!-- identity transform -->
<xsl:template match="@*|node()">
	<xsl:copy>
		<xsl:apply-templates select="@*|node()"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="AdresDoDoreczen | Zakazy | SpolkiCywilneKtorychWspolnikiemJestPrzedsiebiorca" />
<xsl:template match="PrzedsiebiorcaPosiadaObywatelstwaPanstw | MalzenskaWspolnoscMajatkowa" />
<xsl:template match="AdresPelnomocnika | Pelnomocnik" />

<xsl:template match="InformacjaOWpisie">
	<tabela_z_danymi_z_xml>
		<xsl:apply-templates select="@*|node()" />
	</tabela_z_danymi_z_xml>
</xsl:template>

<xsl:template match="AdresGlownegoMiejscaWykonywaniaDzialalnosci/Lokal">
	<xsl:apply-templates select="node()|@*" />
</xsl:template>

<!-- Creates a new 'Lokal_test' element if no 'Lokal' element exists -->
<xsl:template match="AdresGlownegoMiejscaWykonywaniaDzialalnosci[not(Lokal)]">
	<!-- Here the <xsl:copy> was removed -->
	<xsl:apply-templates select="node()/following-sibling::KodPocztowy/preceding-sibling::*|@*" />
	<!-- Copy nodes before 'Wojewodztwo' -->
	<Lokal />
	<xsl:apply-templates select="Budynek/following-sibling::*|@*" />
	<!-- Copy nodes after 'Wojewodztwo' (including) -->
  <!-- Here the <xsl:copy> was removed -->
</xsl:template>

<xsl:template match="DanePodstawowe | DaneKontaktowe | DaneAdresowe | AdresGlownegoMiejscaWykonywaniaDzialalnosci | DaneDodatkowe">
	<xsl:apply-templates select="@*|node()" />
</xsl:template>



	</xsl:stylesheet>